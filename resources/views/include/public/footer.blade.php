<!-- footer section -->

<div class="modal fade bd-example-modal-lg show" id="myModal" role="dialog">
    <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="dynamic-content">

                    <img src="{{ asset('books/preview/1.jpg')  }}" class="img-fluid" alt=""/>
                    <img src="{{ asset('books/preview/2.jpg') }}" class="img-fluid" alt=""/>
                    <img src="{{ asset('books/preview/3.jpg') }}" class="img-fluid" alt=""/>
                    <img src="{{ asset('books/preview/4.jpg') }}" class="img-fluid" alt=""/>
                    <img src="{{ asset('books/preview/5.jpg') }}" class="img-fluid" alt=""/>
                </div>
            </div>
       </div>
</div> 

