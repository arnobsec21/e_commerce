        <!-- DEFAULT WEB VIEW -->
        
        <div class="site-header d-none d-lg-block">
            <div class="header-middle pt--10">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-3 ">
                            <a href="/" class="site-brand">
                                <img src="{{ asset('front_end/images/ebook-GIF100bold.gif') }} " alt="" height="60">
                            </a>
                        </div>
                        <div class="col-lg-6">
                            <div class="header-search-block">
                                <input type="text" placeholder="Search entire store here">
                                <button>Search</button>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="main-navigation flex-lg-right">
                                <div class="cart-widget">
                                    <div class="login-block">
                                        <a href="" class="font-weight-bold"><i class="fas fa-lock"></i> Subscribe </a> <br>
                                    </div>
                                    <div class="cart-block">
                                        <div class="cart-total">
                                            <span class="text-number">
                                                1
                                            </span>
                                           
                                        </div>
                                        
                                    </li>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom">
                <div class="container">
                    <div class="row align-items-center">                        
                        <div class="col-lg-12">
                            <div class="main-navigation flex-lg-left">
                                <ul class="main-menu menu-left li-last-0">
                                    <li class="menu-item has-children mega-menu">
                                        <a href="javascript:void(0)">প্রকাশনী <i class="fas fa-chevron-down dropdown-arrow"></i></a>
                                        <ul class="sub-menu four-column">
                                            @if(!$publisher->isEmpty() && $publisher->count() == 20)
                                                    @foreach($publisher as $val)
                                                        <li class="cus-col-25"><a href="{{ route('details.author',$val->id) }}"> {{ $val->name_bangla}} </a></li>
                                                    @endforeach

                                                    <div  class="pt--10"  >
                                                        <a href="{{ route('all.publisher') }}" class="btn btn-outlined--primary " tabindex="-1">
                                                           See More
                                                        </a>
                                                    </div>
                                            @endif


                                        </ul>
                                    </li>
                                    <!-- Shop -->
                                    <li class="menu-item has-children mega-menu">
                                        <a href="javascript:void(0)">লেখক  <i class="fas fa-chevron-down dropdown-arrow"></i></a>
                                        <ul class="sub-menu four-column">

                                          

                                             @if(!$author->isEmpty() && $author->count() == 20)
                                                    @foreach($author as $val)
                                                        {{-- <li class="cus-col-25"> {{ $val->name_bangla}} </li> --}}
                                                        <li class="cus-col-25"><a href="{{ route('details.author',$val->id) }}"> {{ $val->name_bangla}} </a></li>
                                                    @endforeach
                                                    <div  class="pt--10"  >
                                                        <a href="{{ route('all.author') }}" class="btn btn-outlined--primary " tabindex="-1">
                                                           See More
                                                        </a>
                                                    </div>
                                            @endif
                                           
                                            
                                        </ul>
                                    </li>
                                    <!-- Pages -->
                                    <li class="menu-item">
                                        <a href="{{ route('all.book') }}">সকল বই </a>
                                    </li>

                                  

                                    <li class="menu-item">
                                        <a href="#" class="price-old">বিষয়াবলী</a>
                                    </li>

                                    <li class="menu-item">
                                        <a href="#" class="price-old">বেস্ট সেলার বই</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="#" class="price-old">ভর্তি ও নিয়োগ পরীক্ষা</a>
                                    </li>

                                    <!-- <li class="menu-item">
                                        <a href="contact.html">সায়েন্স ফিকশন বই</a>
                                    </li>

                                    <li class="menu-item">
                                        <a href="contact.html">ইসলামিক বই</a>
                                    </li>

                                     <li class="menu-item">
                                        <a href="contact.html">অনুবাদ বই</a>
                                    </li> -->
                                     <li class="menu-item">
                                        <a href="" class="price-old">অতিরিক্ত ছাড়ের বই</a>
                                    </li>                                    
                                    

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
