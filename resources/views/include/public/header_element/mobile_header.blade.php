
        <!--- mobile view header --->
        <div class="site-mobile-menu">

            <!-- search + logo + cart -->

            <header class="mobile-header d-block d-lg-none pt--10 pb-md--10">
                <div class="container">
                    <div class="row align-items-sm-end align-items-center">
                        <div class="col-md-4 col-7">
                            <a href="index.html" class="site-brand">
                                <img src="{{ asset('front_end/images/ebook-GIF100bold.gif') }} " alt="" height="60">
                            </a>
                        </div>
                        <div class="col-md-5 order-3 order-md-2">
                            <nav class="category-nav   ">
                                <div class="header-search-block">
                                    <input type="text" placeholder="Search entire store here">
                                    <button>Search</button>
                                </div>
                            </nav>
                        </div>

                        <div class="col-md-3 col-5  order-md-3 text-right">
                            <div class="mobile-header-btns header-top-widget">
                                <ul class="header-links">
                                    <li class="sin-link">
                                        <a href="cart.html" class="cart-link link-icon"><i class="ion-bag"></i></a>
                                    </li>
                                    <li class="sin-link">
                                        <a href="javascript:" class="link-icon hamburgur-icon off-canvas-btn"><i
                                                class="ion-navicon"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!--Off Canvas Navigation Start-->
            <aside class="off-canvas-wrapper">
                <div class="btn-close-off-canvas">
                    <i class="ion-android-close"></i>
                </div>
                <div class="off-canvas-inner">
                    <!-- search box start -->
                    
                    <!-- search box end -->
                    <!-- mobile menu start -->
                    <div class="mobile-navigation">
                        <!-- mobile menu navigation start -->
                        <nav class="off-canvas-nav">
                            <ul class="mobile-menu main-mobile-menu">
                                <li class="menu-item-has-children">
                                    <a href="#">লেখক</a>
                                    <ul class="sub-menu">
                                        <li><a href="">Name --- </a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="contact.html">বেস্ট সেলার বই</a>
                                </li>

                                <li>
                                    <a href="contact.html">ভর্তি ও নিয়োগ পরীক্ষা</a>
                                </li>
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->
                    <nav class="off-canvas-nav">
                        <ul class="mobile-menu menu-block-2">
                            
                            
                        </ul>
                    </nav>
                    
                </div>
            </aside>
            <!--Off Canvas Navigation End-->
        </div>
