<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Book Store</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Use Minified Plugins Version For Fast Page Load -->
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('front_end/css/plugins.css')}} " />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('front_end/css/main.css') }} " />
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('front_end/image/favicon.ico') }} ">
</head>

<body>
    <div class="site-wrapper" id="top">

      @include('include.public.header')
      @yield('content')
      @include('include.public.footer')

   </div>  

  @include('include.public.scripts')
  @yield('scripts')

 </body>
</html>
