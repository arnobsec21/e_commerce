@extends('layouts.public_layout')

@section('content')

    @include('include.public.slider')

    <section class="section-margin light-green-background">
       
        <div class="container light-white-background pt--20 mb--20">

            <h4 class="text-center " > [ জনপ্রিয় বই সমূহ ] </h4>

            <div class="product-slider  sb-slick-slider light-white-background" data-slick-setting='{
                            "autoplay": true,
                            "autoplaySpeed": 8000,
                            "slidesToShow": 6,
                            "dots":true
                            }' data-slick-responsive='[
                            {"breakpoint":1400, "settings": {"slidesToShow": 4} },
                            {"breakpoint":992, "settings": {"slidesToShow": 3} },
                            {"breakpoint":768, "settings": {"slidesToShow": 2} },
                            {"breakpoint":575, "settings": {"slidesToShow": 2} },
                            {"breakpoint":490, "settings": {"slidesToShow": 1} }
                        ]'>
                            @if(!$popular->isEmpty())
                            @foreach($popular as $val)
                            <div class="single-slide" style="padding: 5px;">

                                <div class="product-card">
                                    
                                   <div class="product-card--body">
                                        <div class="card-image">
                                            <a href="{{ route('details.book',$val->id) }}" >
                                                <img src="{{ asset('books/images/'.$val->image_url) }}" alt="" class="image-fluid" height="220">
                                            </a>
                                                <div class="hover-contents" style="background-color: #f7f7f761;">

                                                    <div class="hover-btns">

                                                        <a href="" class="single-btn" >
                                                            <i class="fas fa-shopping-basket"></i>
                                                        </a>
                                                        
                                                        <a href="{{ route('details.book',$val->id) }}"  class="single-btn">
                                                            <i class="fas fa-eye"></i>
                                                        </a>
                                                    </div>
                                                    
                                                </div>

                                                <span class="price-discount" style="position:absolute;left:0px;">&#2547; 20  টাকা </span>
                                        </div>
                                        <div class="price-block" style="margin-top:6px;">
                                            <p class="price" style="margin:0px;padding: 0px;font-size: 16px;font-style: oblique;font-weight:normal;">{{ $val->name_bangla}} </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        
            </div>
        </div> 


        <div class="container light-white-background  pt--20 mb--20">

            <h4 class="text-center " > [ বেস্ট সেলার বই সমূহ ] </h4>

            <div class="product-slider  sb-slick-slider light-white-background" data-slick-setting='{
                            "autoplay": true,
                            "autoplaySpeed": 8000,
                            "slidesToShow": 6,
                            "dots":true
                            }' data-slick-responsive='[
                            {"breakpoint":1400, "settings": {"slidesToShow": 4} },
                            {"breakpoint":992, "settings": {"slidesToShow": 3} },
                            {"breakpoint":768, "settings": {"slidesToShow": 2} },
                            {"breakpoint":575, "settings": {"slidesToShow": 2} },
                            {"breakpoint":490, "settings": {"slidesToShow": 1} }
                        ]'>
                            @if(!$bestseller->isEmpty())
                            @foreach($bestseller as $val)
                            <div class="single-slide" style="padding: 5px;">

                                <div class="product-card">
                                    
                                   <div class="product-card--body">
                                        <div class="card-image">
                                            <a href="{{ route('details.book',$val->id) }}" >
                                                <img src="{{ asset('books/images/'.$val->image_url) }}" alt="" class="image-fluid" height="220">
                                            </a>
                                                <div class="hover-contents" style="background-color: #f7f7f761;">

                                                    <div class="hover-btns">

                                                        <a href="" class="single-btn" >
                                                            <i class="fas fa-shopping-basket"></i>
                                                        </a>
                                                        
                                                        <a href="{{ route('details.book',$val->id) }}"  class="single-btn">
                                                            <i class="fas fa-eye"></i>
                                                        </a>
                                                    </div>
                                                    
                                                </div>

                                                <span class="price-discount" style="position:absolute;left:0px;">&#2547; 20  টাকা </span>
                                        </div>
                                        <div class="price-block" style="margin-top:6px;">
                                            <p class="price" style="margin:0px;padding: 0px;font-size: 16px;font-style: oblique;font-weight:normal;">{{ $val->name_bangla}} </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        
            </div>
        </div>    

        <div class="container light-white-background  pt--20 mb--20">

            <h4 class="text-center" > [ সাম্প্রতিক বই সমূহ ] </h4>

            <div class="product-slider  sb-slick-slider light-white-background" data-slick-setting='{
                            "autoplay": true,
                            "autoplaySpeed": 8000,
                            "slidesToShow": 6,
                            "dots":true
                            }' data-slick-responsive='[
                            {"breakpoint":1400, "settings": {"slidesToShow": 4} },
                            {"breakpoint":992, "settings": {"slidesToShow": 3} },
                            {"breakpoint":768, "settings": {"slidesToShow": 2} },
                            {"breakpoint":575, "settings": {"slidesToShow": 2} },
                            {"breakpoint":490, "settings": {"slidesToShow": 1} }
                        ]'>
                            @if(!$recent->isEmpty())
                            @foreach($recent as $val)
                            <div class="single-slide" style="padding: 5px;">

                                <div class="product-card">
                                    
                                   <div class="product-card--body">
                                        <div class="card-image">
                                            <a href="{{ route('details.book',$val->id) }}" >
                                                <img src="{{ asset('books/images/'.$val->image_url) }}" alt="" class="image-fluid" height="220">
                                            </a>
                                                <div class="hover-contents" style="background-color: #f7f7f761;">

                                                    <div class="hover-btns">

                                                        <a href="" class="single-btn" >
                                                            <i class="fas fa-shopping-basket"></i>
                                                        </a>
                                                        
                                                        <a href="{{ route('details.book',$val->id) }}"  class="single-btn">
                                                            <i class="fas fa-eye"></i>
                                                        </a>
                                                    </div>
                                                    
                                                </div>

                                                <span class="price-discount" style="position:absolute;left:0px;">&#2547; 20  টাকা </span>
                                        </div>
                                        <div class="price-block" style="margin-top:6px;">
                                            <p class="price" style="margin:0px;padding: 0px;font-size: 16px;font-style: oblique;font-weight:normal;">{{ $val->name_bangla}} </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        
            </div>
        </div> 
    </section>
@endsection
