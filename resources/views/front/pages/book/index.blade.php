@extends('layouts.public_layout')

@section('content')
<section class="section-margin">
    <div class="container">

    	<div class="section-title section-title--bordered">
            <h2>ALL BOOKS</h2>
        </div>
        <div class="shop-product-wrap with-pagination row space-db--30 shop-border grid-four">

        	@if(!$allbook->isEmpty())
        	@foreach($allbook as $val)
	       		<div class="col-lg-4 col-sm-6">
					<div class="product-card">
						<div class="product-grid-content">
							
							<div class="product-card--body">
								<div class="card-image">
										<img src="{{ asset('books/images/'.$val->image_url) }}" alt="">
										<div class="hover-contents" style="background-color: #f7f7f761;">

											<div class="hover-btns">

												<a href="" class="single-btn add-to-cart" >
													<i class="fas fa-shopping-basket"></i>
												</a>
												
												<a href="{{ route('details.book',$val->id) }}" class="single-btn">
													<i class="fas fa-eye"></i>
												</a>
											</div>

										</div>

										<span class="price-discount" style="position:absolute;left:0px; -webkit-transform: rotate(90deg); /* Safari and Chrome */
									    -moz-transform: rotate(-20deg);   /* Firefox */
									    -ms-transform: rotate(-20deg);   /* IE 9 */
									    -o-transform: rotate(-20deg);   /* Opera */
									    transform: rotate(-20deg);">&#2547; 20  টাকা </span>
								</div>
								<div class="price-block" style="margin-top:6px;">
									<p class="price" style="margin:0px;padding: 0px;font-size: 16px;font-style: oblique;font-weight:normal;">{{ $val->name_bangla}} </p>
								</div>
							</div>
						</div>
					
					</div>
				</div>
			@endforeach
			@endif


		</div>

		<div class="row pt--30">
			<div class="col-md-12">
				<div class="pagination-block">

					 @if ($allbook->lastPage() > 1)
				        <ul class="pagination-btns flex-center">
				            <li class="{{ ($allbook->currentPage() == 1) ? ' disabled' : '' }} page-item">
				                <a class=" page-link " href="{{ $allbook->url(1) }}" aria-label="Previous">
				                    <span aria-hidden="true">&laquo;</span>
				                    <span class="sr-only">Previous</span>
				                </a>
				            </li>
				           {{ $allbook->links() }}
				            <li class="{{ ($allbook->currentPage() == $allbook->lastPage()) ? ' disabled' : '' }} page-item">
				                <a href="{{ $allbook->url($allbook->currentPage()+1) }}" class="page-link" aria-label="Next">
				                    <span aria-hidden="true">&raquo;</span>
				                    <span class="sr-only">Next</span>
				                </a>
				            </li>
				        </ul>
					@endif

{{-- 
					<ul class="pagination-btns flex-center">
						<li><a href="#" class="single-btn prev-btn ">|<i class="zmdi zmdi-chevron-left"></i> </a></li>
						<li><a href="#" class="single-btn prev-btn "><i class="zmdi zmdi-chevron-left"></i> </a></li>
						<li class="active"><a href="#" class="single-btn">1</a></li>
						<li><a href="#" class="single-btn">2</a></li>
						<li><a href="#" class="single-btn">3</a></li>
						<li><a href="#" class="single-btn">4</a></li>
						<li><a href="#" class="single-btn next-btn"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li><a href="#" class="single-btn next-btn"><i class="zmdi zmdi-chevron-right"></i>|</a></li>
					</ul> --}}
				</div>
			</div>
		</div>
    </div>
</section>
@endsection
@section('scripts')

<script type="text-javascript">

	{{-- $('.add-to-cart').on('click', function () {
	alert();
        var cart = $('.cart-block');
        var imgtodrag = $(this).parent('.card-image').find("img").eq(0);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
            })
                .appendTo($('body'))
                .animate({
                'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
            }, 1000, 'easeInOutExpo');
            
            setTimeout(function () {
                cart.effect("shake", {
                    times: 2
                }, 200);
            }, 1500);

            imgclone.animate({
                'width': 0,
                    'height': 0
            }, function () {
                $(this).detach()
            });
        }
    }); --}}
</script>
@endsection
