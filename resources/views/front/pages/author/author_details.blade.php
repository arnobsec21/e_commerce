@extends('layouts.public_layout')

@section('content')
<section class="section-margin light-green-background pt--20">

	<div class="container ">
		<div class="breadcrumb-contents  pl-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ route('all.author') }}">লেখক </a></li>
					<li class="breadcrumb-item"> বিস্তারিত </li>
					<li class="breadcrumb-item active">
						<i >  লেখকের  নাম </i>  <span style="font-weight: bold"> {{ $authorDetails->name_bangla }} </span> </i>
					</li>
					
				</ol>
			</nav>
		</div>


	</div>

    <div class="container">
    	<div class="shop-product-wrap list with-pagination row space-db--30 shop-border">
					<div class="col-lg-4 col-sm-6">
						<div class="product-card card-style-list">

							<div class="product-list-content">
								<div class="card-image">
									<img src="{{ asset('authors/images/'.$authorDetails->image_url) }}" alt="">
								</div>
								<div class="product-card--body">
									<div class="product-header">
										
									<h3>{{ $authorDetails->name_bangla}}</h3> 
									 
									

									<span> 
									  	<b><i class="fas fa-globe"></i></b>
									  	 <i>  www.website.com
									  	</i>
									</span>
									</div>
									<article>

									<div class="rating-block">
										<span class="fas fa-star star_on"></span>
										<span class="fas fa-star star_on"></span>
										<span class="fas fa-star star_on"></span>
										<span class="fas fa-star star_on"></span>
										<span class="fas fa-star "></span>
									</div>
																		
																			
										<p>	
											{{ $authorDetails->author_bio }} 
											
										</p>
									</article>

									

										<span class="price-discount" style="background: green;padding:3px"> <i class="fa fa-book"></i> বই  ১৪০০   </span>

										
										

								</div>
							</div>
						</div>
					</div>

				</div>
	</div>

	<div class="container light-white-background ">

	 	<h4 class="text-center pt-4 mt-5 mb-5" > [ {{ $authorDetails->name_bangla }} এর বই সমূহ ] </h4>

		<div class="product-slider  sb-slick-slider light-white-background" data-slick-setting='{
                        "autoplay": true,
                        "autoplaySpeed": 8000,
                        "slidesToShow": 6,
                        "dots":true
                        }' data-slick-responsive='[
                        {"breakpoint":1400, "settings": {"slidesToShow": 4} },
                        {"breakpoint":992, "settings": {"slidesToShow": 3} },
                        {"breakpoint":768, "settings": {"slidesToShow": 2} },
                        {"breakpoint":575, "settings": {"slidesToShow": 2} },
                        {"breakpoint":490, "settings": {"slidesToShow": 1} }
                    ]'>
                    	@if(!$relatedProduct->isEmpty())
        				@foreach($relatedProduct as $val)
	                    <div class="single-slide" style="padding: 5px;">

	                        <div class="product-card">
	                            
	                           <div class="product-card--body">
									<div class="card-image">
										<a href="{{ route('details.book',$val->id) }}" >
											<img src="{{ asset('books/images/'.$val->image_url) }}" alt="" class="image-fluid" height="220">
										</a>
											<div class="hover-contents" style="background-color: #f7f7f761;">

												<div class="hover-btns">

													<a href="" class="single-btn" >
														<i class="fas fa-shopping-basket"></i>
													</a>
													
													<a href="{{ route('details.book',$val->id) }}"  class="single-btn">
														<i class="fas fa-eye"></i>
													</a>
												</div>
												
											</div>

											<span class="price-discount" style="position:absolute;left:0px;">&#2547; 20  টাকা </span>
									</div>
									<div class="price-block" style="margin-top:6px;">
										<p class="price" style="margin:0px;padding: 0px;font-size: 16px;font-style: oblique;font-weight:normal;">{{ $val->name_bangla}} </p>
									</div>
								</div>
	                        </div>
	                    </div>
	                    @endforeach
	                    @endif
                    
        </div>
	</div>
</section>
@endsection
@section('scripts')
@endsection
