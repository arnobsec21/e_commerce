<?php

namespace App\Models\Author;

use Illuminate\Database\Eloquent\Model;

class AuthorModel extends Model
{
    
    //
    protected $table="tbl_author";
    protected $fillable=[ 
    				'unique_identifier',
    				'name',
    				'author_bio',
    				'bio_url',
    				'is_popular',
    				'popular_order',
    				'created_at',
    				'updated_at'

				];
}
