<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 // use Goutte\Client;
 // use Symfony\Component\CssSelector\CssSelectorConverter;

Route::get('/', 'HomeController@index')->name('Home_page_view');

Route::get('/all/author', ['as' => 'all.author','uses' => 'HomeController@allauthor']);
Route::get('/all/publisher', ['as' => 'all.publisher','uses' => 'HomeController@allpublisher']);
Route::get('/all/book', ['as' => 'all.book','uses' => 'HomeController@allbook']);

Route::get('/details/{id}/book', ['as' => 'details.book','uses' => 'HomeController@detailsbook']);
Route::get('/details/{id}/publisher', ['as' => 'details.publisher','uses' => 'HomeController@detailspublisher']);
Route::get('/details/{id}/author', ['as' => 'details.author','uses' => 'HomeController@detailsauthor']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
